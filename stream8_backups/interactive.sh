#!/bin/bash

source common.sh
FUSEMOUNT=fusemount

if [ ! -f /tmp/credentials ]; then
  source get_credentials.sh
fi
source /tmp/credentials

if [ "$1" == "restore" ]; then
  if [ -z $2 ]; then
    echo "Error, need a snapshot [YYYYMMDD] to restore ..."
    exit
  fi
  if [ -z $3 ]; then
    echo "Error, need a target path to restore to ..."
    exit
  fi
  $RESTIC restore latest --tag $2 -i $2 --target "$3"
elif [ "$1" == "snapshots" ]; then
  $RESTIC snapshots
elif [ "$1" == "unlock" ]; then
  $RESTIC unlock
elif [ "$1" == "mount" ]; then
  # TODO: check fusemount is not already mounted
  if [ ! -d "$FUSEMOUNT" ]; then
    mkdir $FUSEMOUNT
  fi
  echo "mounting \"$FUSEMOUNT\" (process backgrounded)"
  echo "use: '$0 unmount' when finished"
  $RESTIC mount $FUSEMOUNT &> /dev/null &
elif [ "$1" == "unmount" ]; then
  pkill -f $RESTIC &> /dev/null
  fusermount -u $FUSEMOUNT &> /dev/null
else
  echo "usage: $0 <restore YYYYMMDD path|unlock|mount|unmount|snapshots>"
fi
